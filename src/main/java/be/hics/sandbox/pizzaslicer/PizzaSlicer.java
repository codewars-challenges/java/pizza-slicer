package be.hics.sandbox.pizzaslicer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class PizzaSlicer {

    public static BigInteger[] slice(final BigInteger pizzas, final BigInteger stomachs) {
        if (pizzas.compareTo(BigInteger.ONE) == 0)
            return new BigInteger[]{stomachs};
        List<BigInteger> denominators = getDenominators(new Fraction(pizzas, stomachs));
        return denominators.toArray(new BigInteger[denominators.size()]);
    }

    private static List<BigInteger> getDenominators(final Fraction fraction) {
        List<BigInteger> denominators = new ArrayList<>();
        Tuple<BigInteger, Fraction> tuple = calculateNext(fraction);
        denominators.add(tuple.getX());
        while (tuple.getY().getNumerator().compareTo(BigInteger.ONE) > 0) {
            tuple = calculateNext(tuple.getY());
            denominators.add(tuple.getX());
        }
        denominators.add(tuple.getY().getDenominator());
        return denominators;
    }

    private static Tuple<BigInteger, Fraction> calculateNext(final Fraction fraction) {
        BigInteger baseDenominator = calculateBaseDenominator(fraction);
        Fraction baseFraction = new Fraction(BigInteger.ONE, baseDenominator);
        Fraction restFraction = calculateRestFraction(fraction, baseDenominator);
        return new Tuple<>(baseFraction.getDenominator(), restFraction);
    }

    private static BigInteger calculateBaseDenominator(final Fraction fraction) {
        BigDecimal numerator = new BigDecimal(fraction.getNumerator());
        BigDecimal denominator = new BigDecimal(fraction.getDenominator());
        BigDecimal ceiling = (denominator).divide(numerator, RoundingMode.CEILING);
        return ceiling.toBigInteger();
    }

    private static Fraction calculateRestFraction(final Fraction fraction, final BigInteger baseDenominator) {
        BigInteger denominator = baseDenominator.multiply(fraction.getDenominator());
        BigInteger numerator = baseDenominator.multiply(fraction.getNumerator()).subtract(fraction.getDenominator());
        return new Fraction(numerator, denominator).simplify();
    }

    private static class Tuple<X, Y> {

        private final X x;
        private final Y y;

        protected Tuple(final X x, final Y y) {
            this.x = x;
            this.y = y;
        }

        protected X getX() {
            return x;
        }

        protected Y getY() {
            return y;
        }

    }

    private static class Fraction extends Tuple<BigInteger, BigInteger> {

        protected Fraction(BigInteger numerator, BigInteger denominator) {
            super(numerator, denominator);
        }

        protected BigInteger getNumerator() {
            return getX();
        }

        protected BigInteger getDenominator() {
            return getY();
        }

        protected Fraction simplify() {
            if (getNumerator().compareTo(BigInteger.ZERO) == 0 || getNumerator().compareTo(BigInteger.ONE) == 0)
                return this;
            BigInteger gcd = getNumerator().gcd(getDenominator());
            if (gcd.compareTo(BigInteger.ONE) == 0)
                return this;
            return new Fraction(getNumerator().divide(gcd), getDenominator().divide(gcd));
        }

    }

}
