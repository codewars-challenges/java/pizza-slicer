package be.hics.sandbox.pizzaslicer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class PizzaSlicerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PizzaSlicerApplication.class, args);
        prettyPrintPizzaSlicer(2,3);
        prettyPrintPizzaSlicer(3,4);
        prettyPrintPizzaSlicer(4,6);
    }

    private static void prettyPrintPizzaSlicer(final long pizzas, final long stomachs) {
        System.out.println("---");
        Arrays.stream(PizzaSlicer.slice(BigInteger.valueOf(pizzas), BigInteger.valueOf(stomachs))).forEach(System.out::println);
    }

}
