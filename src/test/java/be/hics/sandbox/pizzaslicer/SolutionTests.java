package be.hics.sandbox.pizzaslicer;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class SolutionTests {

    private void testPizzaSlicer(final BigInteger pizzas, final BigInteger stomachs) {
        BigInteger[] slices = PizzaSlicer.slice(pizzas, stomachs);
        BigInteger retrofittedStomachs = Arrays.stream(slices).reduce(BigInteger.ONE, BigInteger::multiply);
        BigInteger retrofittedPizzas = Arrays.stream(slices).map(retrofittedStomachs::divide).reduce(BigInteger.ZERO, BigInteger::add);
        BigDecimal result = (new BigDecimal(retrofittedPizzas)).divide(new BigDecimal(retrofittedStomachs), 10, RoundingMode.HALF_UP);
        BigDecimal expected = (new BigDecimal(pizzas)).divide(new BigDecimal(stomachs), 10, RoundingMode.HALF_UP);
        String message = String.format("Invalid slicing of %s pizza's for %s stomachs", pizzas.toString(), stomachs.toString());
        assertTrue(message, result.compareTo(expected) == 0);
    }

    @Test
    public void test01() {
        testPizzaSlicer(BigInteger.valueOf(321L), BigInteger.valueOf(12345L));
    }

    @Test
    public void test02() {
        testPizzaSlicer(BigInteger.valueOf(6544565465L), BigInteger.valueOf(98798726465551654L));
    }

    @Test
    public void test03() {
        testPizzaSlicer(BigInteger.valueOf(1L), BigInteger.valueOf(2L));
    }

    @Test
    public void test04() {
        testPizzaSlicer(BigInteger.valueOf(1L), BigInteger.valueOf(2L));
    }

    private void randomTest(final long lowerLimit, final long upperLimit) {
        long random1 = lowerLimit + (long) (Math.random() * (upperLimit - lowerLimit));
        long random2 = lowerLimit + (long) (Math.random() * (upperLimit - lowerLimit));
        if (random1 == random2) {
            random1 = random2 - 1;
        }
        if (random1 < random2)
            testPizzaSlicer(BigInteger.valueOf(random1), BigInteger.valueOf(random2));
        else
            testPizzaSlicer(BigInteger.valueOf(random2), BigInteger.valueOf(random1));
    }

    @Test
    public void test05() {
        randomTest(1L, 100000L);
    }

    @Test
    public void test06() {
        randomTest(1L, 100000L);
    }

    @Test
    public void test07() {
        randomTest(1L, 100000L);
    }

    @Test
    public void test08() {
        randomTest(1000L, 1000000000L);
    }

    @Test
    public void test09() {
        randomTest(1000L, 1000000000L);
    }


    @Test
    public void test10() {
        randomTest(1000L, 1000000000L);
    }

    @Test
    public void test11() {
        randomTest(1000000L, 1000000000000000000L);
    }

    @Test
    public void test12() {
        randomTest(1000000L, 1000000000000000000L);
    }

    @Test
    public void test13() {
        randomTest(1000000L, 1000000000000000000L);
    }

    @Test
    public void test14() {
        randomTest(1000000L, 1000000000000000000L);
    }

    @Test
    public void test15() {
        randomTest(1000000L, 1000000000000000000L);
    }

    @Test
    public void test16() {
        randomTest(1000000L, 1000000000000000000L);
    }

    @Test
    public void test17() {
        randomTest(1000000000L, 1000000000000000000L);
    }

    @Test
    public void test18() {
        randomTest(1000000000L, 1000000000000000000L);
    }

    @Test
    public void test19() {
        randomTest(1000000000L, 1000000000000000000L);
    }

    @Test
    public void test20() {
        randomTest(1L, 1000000000000000000L);
    }
}
