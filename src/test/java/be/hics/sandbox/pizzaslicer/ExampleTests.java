package be.hics.sandbox.pizzaslicer;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertArrayEquals;

public class ExampleTests {

    @Test
    public void test1() {
        assertArrayEquals(PizzaSlicer.slice(BigInteger.valueOf(2), BigInteger.valueOf(3)), new BigInteger[]{BigInteger.valueOf(2), BigInteger.valueOf(6)});
    }

    @Test
    public void test2() {
        assertArrayEquals(PizzaSlicer.slice(BigInteger.valueOf(3), BigInteger.valueOf(4)), new BigInteger[]{BigInteger.valueOf(2), BigInteger.valueOf(4)});
    }

    @Test
    public void test3() {
        assertArrayEquals(PizzaSlicer.slice(BigInteger.valueOf(4), BigInteger.valueOf(6)), new BigInteger[]{BigInteger.valueOf(2), BigInteger.valueOf(6)});
    }

    @Test
    public void test4() {
        assertArrayEquals(PizzaSlicer.slice(BigInteger.valueOf(3), BigInteger.valueOf(13)), new BigInteger[]{BigInteger.valueOf(5), BigInteger.valueOf(33), BigInteger.valueOf(2145)});
    }

}
