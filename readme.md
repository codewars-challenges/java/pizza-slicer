# The Pizza Slicer
### Situation
A few minutes ago I ordered 2 large spicy BBQ pizza's for my two kids and myself.  But I need to solve a common problem before the delivery guy arrives on my doorstep.  I need to find a simple but correct way to slice the pizza's in parts to avoid every discussion.

### The simple solution is obvious
2 pizza's divided by 3 people results in 2/3 of pizza per person.  But I dare you to divide 2 pizza's in 3 parts of 2/3 without making someone unhappy.  Obviously I'm clearly not satisfied with this solution; I need a method to slice the pizza's evenly.

Something like this:<br/>
```
2/3 = 4/6 = 3/6 + 1/6 = 1/2 + 1/6
```
Everybody get's 1/2 + 1/6 of all pizza's, which is easier to slice evenly.

### Rules
Let's generalize this in following challenge.

###### Given:
* <b>pizzas</b>: amount of pizza's (as non null, positive BigInteger, greater than zero)
* <b>stomachs</b>: amount of stomachs to feed (also non null, positive BigInteger, greater than zero)
* amount of stomachs is always greater than amount of pizza's  
* <i>one person only has one stomach, let's exclude pizza eating cows having 4 stomachs!</i>

###### When:
* pizza's are sliced

###### Then:
* <b>slices</b>: an array (of BigInteger) is expected with distinct slices
* <b>distinct</b>: the slices need to be different;  2/3 can also be sliced as 1/3 + 1/3, but this is not considered a valid solution.
* <b>remarks</b>: multiple slicing patterns can be valid for one combination of pizza's and stomachs 

### Example
For the previous example this would result in following:
```
PizzaSlicer.slice(2 /*pizzas*/, 3 /*stomachs*/) => [ 2, 6 ]
```
